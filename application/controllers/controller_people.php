<?php 
class Controller_People extends Controller
{

	function __construct()
	{
		$this->model = new Model_People();
		$this->view = new View();
	}
    function action_spec($id)
	{	     

		$data = $this->model->get_data_par($id);		
		$this->view->generate('people_view.php', 'template_view.php', $data);
	}
}
